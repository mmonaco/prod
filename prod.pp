
node default {
	include base
}

node /ns[1-3]\.monaco\.cx/ {
	include base
	include pdns

	file {"/etc/motd":
		content => @(EOF),
		sudo pdnsutil create-slave-zone <zone> <master-ip...>
		sudo pdnsutil import-tsig-key <keyname> <hmac> <key>
		sudo pdnsutil activate-tsig-key <zone> <keyname> slave

		sudo sqlite3 /var/lib/powerdns/db.sqlite3 "SELECT * FROM domains"
		| EOF
	}

	file {"/etc/nftables.conf":
		owner   => "root",
		group   => "root",
		mode    => "u=rw,go=r",
		content => @(EOF),
		#!/usr/bin/nft -f
		# ipv4/ipv6 Simple & Safe Firewall
		# you can find examples in /usr/share/nftables/

		table inet filter {
		  chain input {
		    type filter hook input priority 0;

		    # allow established/related connections
		    ct state {established, related} counter accept

		    # early drop of invalid connections
		    ct state invalid counter drop

		    # allow from loopback
		    iifname lo counter accept

		    # allow icmp
		    ip protocol icmp   counter accept
		    ip6 nexthdr icmpv6 counter accept

		    # allow ssh (custom) and dns
		    tcp dport 22002 counter accept comment "SSH"
		    tcp dport 53    counter accept comment "DNS (tcp)"
		    udp dport 53    counter accept comment "DNS (udp)"

		    # everything else
		    counter reject with icmpx type port-unreachable
		  }
		  chain forward {
		    type filter hook forward priority 0;
		    counter drop
		  }
		  chain output {
		    type filter hook output priority 0;
		    ct state invalid counter drop
		  }

		}
		# vim:set ts=2 sw=2 et:
		| EOF
	}
	~>
	service {"nftables.service":
		ensure => running,
		enable => true,
	}
}

class base {
	include archlinux::pacman
	include ssh
	include matt

	package {[
		"bash-completion",
		"bind-tools",
		"curl",
		"git",
		"inetutils",
		"iproute2",
		"man-db",
		"net-tools",
		"nftables",
		"puppet",
		"rsync",
		"strace",
		"sudo",
		"tmux",
		"tree",
		"vim",
		]:
		ensure => installed,
	}

	service {[
		"systemd-userdbd.service",
		"systemd-userdbd.socket",
		"systemd-homed.service",
		]:
		ensure => stopped,
		enable => mask,
	}

}

class ssh {
	package {"openssh":
		ensure => installed,
	}
	->
	file_line {
		"/etc/ssh/sshd_config Port=22002":
			path  => "/etc/ssh/sshd_config",
			line  => "Port 22002",
			match => "^Port +",
			after => "^#Port +",
		;
		"/etc/ssh/sshd_config PermitRootLogin=no":
			path  => "/etc/ssh/sshd_config",
			line  => "PermitRootLogin no",
			match => "^PermitRootLogin +",
			after => "^#PermitRootLogin +",
		;
		"/etc/ssh/sshd_config PasswordAuthentication=no":
			path  => "/etc/ssh/sshd_config",
			line  => "PasswordAuthentication no",
			match => "^PasswordAuthentication +",
			after => "^#PasswordAuthentication +",
		;
	}
	~> service {"sshd.service":
		ensure => running,
		enable => true,
	}
}

class matt {
	user {"$name":
		uid            => 1000,
		shell          => "/bin/bash",
		groups         => ["systemd-journal"],
		membership     => "inclusive",
		password       => "!",
		system         => false,
		purge_ssh_keys => true,
		home           => "/home/$name",
	}

	file {"/etc/sudoers.d/$name":
		owner   => "root",
		group   => "root",
		mode    => "ug=r,o=",
		content => "$name ALL=(ALL) NOPASSWD: ALL\n",
	}

	ssh_authorized_key {"Matthew Monaco <matt@monaco.cx>":
		user => "$name",
		type => "ssh-rsa",
		key  => "AAAAB3NzaC1yc2EAAAADAQABAAABAQC+S/+/1AuHuHCMUfs/OFycCqIbGWGm77iR6Lkr5ScROD7C+gwAncRRjVtHExtbYlzP2Md6SzgsOjEPBG3TIsx5UG17UddELoSTaYGWxxe5Nw3IFVoodBctvM5YLzPZEHd6K1DWU2yXD8PFhpNwNOAnnLHnSBWJ+v1PcVUgQm92uDV97cZQLBxUzh9pzRsfPhb3eegNrA3CYgZ3TU8Bzh8mvKmyOSUisffd1k6g2BZzMf+7REXN4V21y8OrNMsSmEld7JxJMIE6/UgR36IQ3GqitOQze0lHgOfEqs98lYeLQBPZfNslXLeZ2id1kROuKh4dqCF8stWOqADTQTPDP8Tf",
	}

	file {"/home/$name/.config/":
		ensure => directory,
		owner  => "$name",
		mode   => "u=rwx,g=rx,go=",
	}
		
	exec {"/usr/bin/git clone --bare https://gitlab.com/mmonaco/dotfiles.git":
		cwd     => "/home/$name/.config/",
		user    => "$name",
		creates => "/home/$name/.config/dotfiles.git",
	}
	->
	exec {"/usr/bin/git --git-dir=.config/dotfiles.git --work-tree=. reset --hard":
		cwd     => "/home/$name/",
		user    => "$name",
		creates => "/home/$name/.gitignore",
	}
}


class archlinux::pacman {
	file {"/etc/pacman.d/hooks/pacman-clearcache.hook":
		owner   => "root",
		group   => "root",
		mode    => "u=rw,go=r",
		content => @(EOF),
		[Trigger]
		Operation = Install
		Operation = Upgrade
		Type = Package
		Target = *

		[Action]
		Description = Clearing package cache
		Depends = findutils
		When = PostTransaction
		Exec = /usr/bin/find /var/cache/pacman/pkg/ -type f -delete
		| EOF
	}
	->
	exec { "/usr/bin/pacman --noconfirm -Sy --needed archlinux-keyring":
		refreshonly => true,
	}
	->
	Package<||>
}

class pdns {

	$db = "/var/lib/powerdns/db.sqlite3"

	file {"/etc/systemd/resolved.conf.d/":
		ensure => directory,
	}
	file {"/etc/systemd/resolved.conf.d/nostub.conf":
		content => @(EOF),
		[Resolve]
		LLMNR=no
		DNSStubListener=no
		| EOF
	}
	~>
	service {"systemd-resolved.service":
	}

	package {"powerdns":
		ensure => installed,
	}
	->
	file {"/var/lib/powerdns/":
		ensure => directory,
		owner  => "powerdns",
		group  => "powerdns",
		mode   => "u=rwx,g=rx,o=",
	}
	->
	exec {"/usr/bin/sqlite3 '$db' -init /usr/share/doc/powerdns/schema.sqlite3.sql": 
		creates => "$db",
		user    => "powerdns",
	}
	->
	file {"/etc/powerdns/pdns.conf":
		owner   => "root",
		group   => "root",
		mode    => "u=rw,go=r",
		content => @(EOF),
		slave=yes
		launch=gsqlite3
		gsqlite3-database=/var/lib/powerdns/db.sqlite3
		gsqlite3-pragma-foreign-keys
		gsqlite3-dnssec=yes
		setuid=powerdns
		setgid=powerdns
		| EOF
	}
	~>
	service {"pdns.service":
		ensure => running,
		enable => true,
	}
}
