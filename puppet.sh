#!/bin/bash

startdir="$(readlink -f "$(dirname "$0")")"

cmd=(
	ruby -W0
	/usr/bin/puppet apply
		--modulepath "$startdir"/modules:"$startdir"/modules-upstream
		--test
		"$startdir"/prod.pp
		"$@"
)

printf "%s%s%s\n" "$(tput bold)" "${cmd[*]}" "$(tput sgr0)"
exec "${cmd[@]}"
