#!/bin/bash

blk=/dev/vda
root=/mnt

pacman -Sy --needed archlinux-keyring
echo 'Server = https://mirrors.kernel.org/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist

fdisk "$blk" <<EOF
g
n
1
2048
+2M
t
4
x
n
bios
r
n
2


t
2
24
x
n
2
root
r
w
EOF

partprobe "$blk"

mkfs.ext4 -L root "${blk}2"
mount -o noatime LABEL=root "$root"

pacstrap "$root" base linux curl bash-completion tmux vim sudo puppet git
sed -e 's|^#UseSyslog.*|UseSyslog|' -e 's|^#Color.*|Color|' -e 's|^#TotalDownload.*|TotalDownload|' -i "$root"/etc/pacman.conf
sed 's|^#en_US\.UTF-8.*|en_US.UTF-8 UTF-8|' -i "$root"/etc/locale.gen
arch-chroot "$root" locale-gen
echo "LANG=en_US.UTF-8" > "$root"/etc/locale.conf

ln -sf /run/systemd/resolve/resolv.conf  "$root"/etc/resolv.conf
cat > "$root"/etc/systemd/network/default.network <<-EOF
[Match]
Name=*

[Network]
DHCP=both

EOF

sed "s|^PRESETS.*|PRESETS=('default')|" -i "$root"/etc/mkinitcpio.d/linux.preset
sed 's|^MODULES.*|MODULES=(cirrus)|' -i "$root"/etc/mkinitcpio.conf
sed 's|^HOOKS.*|HOOKS=(base systemd autodetect modconf block filesystems keyboard)|' -i "$root"/etc/mkinitcpio.conf
rm "$root"/boot/initramfs-linux-fallback.img
arch-chroot "$root" mkinitcpio -P


arch-chroot "$root" grub-install --target i386-pc "$blk"
rm "$root"/boot/grub/grub.cfg.example
cat > "$root"/boot/grub/grub.cfg <<-EOF
search.fs_label root root

set timeout=2
set default=0

loadfont unicode
insmod vbe
insmod gzio
insmod part_gpt
insmod btrfs

menuentry 'Arch Linux' {
	linux   /boot/vmlinuz-linux root=LABEL=root rw console=ttyS1 console=tty
	initrd  /boot/initramfs-linux.img
}
EOF
