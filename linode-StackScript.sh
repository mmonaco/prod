#!/bin/bash
#
# <UDF name="hostname" label="hostname" />
#
# 1) Basic system and account configuration. Enough that the system is
# comfortable.
#
# 2) Retreive and execute puppet based on hostname. A default base config
# replicates the config in this file, if a hostname-specific node is defined,
# it is used.
#
# Dependencies:
#
#  - Assumes a vanilla, linode-provided, recent Archlinux image.
#  - Assumes linode injected /root/.ssh/authorized_keys 
#  - https://mirrors.kernel.org/archlinux/
#  - https://gitlab.com/mmonaco/dotfiles.git
#  - https://gitlab.com/mmonaco/prod.git (github, etc submodules)
#

### Set Hostname

hostnamectl set-hostname "$HOSTNAME"

### Basic Pacman Init

cat <<-'EOF' > /etc/pacman.d/mirrorlist
Server = https://mirrors.kernel.org/archlinux/$repo/os/$arch
EOF
mkdir /etc/pacman.d/hooks/
cat <<-'EOF' > /etc/pacman.d/hooks/pacma-cleancache.hook
[Trigger]
Operation = Install
Operation = Upgrade
Type = Package
Target = *

[Action]
Description = Cleaning package cache
Depends = findutils
When = PostTransaction
Exec = /usr/bin/find /var/cache/pacman/pkg/ -type f -delete
EOF
pacman --noconfirm -Sy
pacman --noconfirm -S --needed archlinux-keyring
pacman --noconfirm -S --needed -u sudo git puppet bash-completion tmux

### Setup matt

useradd -mU -u 1000 -G systemd-journal -s /bin/bash matt
install -m0440 <(echo "matt ALL=(ALL) NOPASSWD: ALL") /etc/sudoers.d/matt
install -m0750 -omatt -gmatt -d /home/matt/.ssh/
install -m0640 -omatt -gmatt /root/.ssh/authorized_keys /home/matt/.ssh/

### Setup matt's dotfiles

cd /home/matt

sudo -u matt mkdir .config/
sudo -u matt git clone --bare https://gitlab.com/mmonaco/dotfiles.git .config/dotfiles.git
sudo -u matt git --git-dir=.config/dotfiles.git --work-tree=. reset --hard

### Setup puppet for everything else

sudo -u matt git clone --recurse-submodules https://gitlab.com/mmonaco/prod.git
/home/matt/prod/puppet.sh

echo "############################"
echo "### DONE... REBOOTING!   ###"
echo "############################"
reboot
